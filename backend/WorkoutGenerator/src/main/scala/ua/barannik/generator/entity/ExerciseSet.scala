package ua.barannik.generator.entity

abstract class ExerciseSet

case class Reps(reps: Int) extends ExerciseSet {
  require(reps > 0)
}

case class WeightAndReps(reps: Int, weight: Float) extends ExerciseSet {
  require(reps > 0 && weight >= 0)
}
