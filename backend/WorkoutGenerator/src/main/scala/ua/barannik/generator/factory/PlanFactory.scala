package ua.barannik.generator.factory

import ua.barannik.generator.entity.Exercise

case class PlanFactory[E <: Exercise[_]](exerciseFactory: ExerciseFactory[E]) {
  def create(startLevel: Float, step: Float, number: Int): Seq[E] =
    (0 until number).view
      .map(i => startLevel + step * i)
      .map(exerciseFactory.create)
      .force

  def create(startLevel: Float, number: Int): Seq[E] = exerciseFactory.asInstanceOf[Any] match {
    case ClassicalExerciseFactory(work, _, _) => create(startLevel + 0.0001f, 1f / work, number)
    case _ => create(startLevel, 1, number)
  }
}