package ua.barannik.generator.entity

import scala.language.implicitConversions

abstract class Section[+T <: ExerciseSet](_sets: Seq[T]) extends Seq[ExerciseSet] {
  override def length = _sets.length
  override def apply(idx: Int) = _sets.apply(idx)
  override def iterator = _sets.iterator
}

case class WarmUp[+T <: ExerciseSet](sets: Seq[T] = Nil) extends Section[T](sets)
case class Work[+T <: ExerciseSet](sets: Seq[T] = Nil) extends Section[T](sets)
case class End[+T <: ExerciseSet](sets: Seq[T] = Nil) extends Section[T](sets)
case class Empty[+T <: ExerciseSet]() extends Section[T](Nil)

object SectionConversions {
  implicit def Seq2WarmUp[T <: ExerciseSet](seq: Seq[T]): WarmUp[T] = WarmUp(seq)
  implicit def Seq2Work[T <: ExerciseSet](seq: Seq[T]): Work[T] = Work(seq)
  implicit def Seq2End[T <: ExerciseSet](seq: Seq[T]): End[T] = End(seq)
  implicit def Nil2Empty[T <: ExerciseSet](seq: List[Nothing]) = Empty
}