package ua.barannik.generator.entity

object Declarative {
  trait weightUnits[T <: AnyVal] {
    def value: T
    val kg: T = value
  }
  implicit class weightUnitsAny[T <: AnyVal](override val value: T) extends weightUnits[T]

  implicit class weightUnitsFloat(input: Double) extends weightUnits[Float] {
    def value: Float = input.toFloat
  }
}
