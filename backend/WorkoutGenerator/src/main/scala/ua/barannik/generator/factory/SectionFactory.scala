package ua.barannik.generator.factory

import ua.barannik.generator.entity.{Exercise, ExerciseSet}

abstract class SectionFactory[T <: ExerciseSet] {
  def create(level: Float): Seq[T]
}

/** For example we have setsAmount = 3.
  * In case level = 5, all sets will have level 5.
  * In case level = 5.4, first set will have level 6, second and third level = 5
  * In case level = 5.8 first and second set will have level = 6, third level = 5
  * In case level 6, all sets will have level 6
  */
case class UniformSectionFactory[T <: ExerciseSet](setsAmount: Int, setFactory: SetFactory[T])
  extends SectionFactory[T] {

  override def create(level: Float): Seq[T] = {
    val decimalPart = level - level.toInt
    val increasedLevelSetsAmount = (decimalPart * setsAmount).toInt

    for (i <- 1 to setsAmount;
         isIncreased = i <= increasedLevelSetsAmount)
      yield setFactory.create(level.toInt + (if (isIncreased) 1 else 0))
  }
}

case class FloatLevelSectionFactory[T <: ExerciseSet](setFactory: SetFactory[T], factors: Float*)
  extends SectionFactory[T] {
  require(!factors.exists(v => v < 0 || v > 1))

  override def create(level: Float): Seq[T] = factors.map(f => setFactory.create((f * level).toInt))
}

case class MountainSectionFactory[T <: ExerciseSet]
  (_minPercent: Float, _maxPercent: Float, amount: Int, setFactory: SetFactory[T])
  extends SectionFactory[T] {
  require(_minPercent >= 0 && _minPercent <= 1 && _maxPercent >=0 && _maxPercent <= 1 && amount > 0)

  val isLevelIncreasing: Boolean = _maxPercent >= _minPercent

  private[this] val __maxPercent = if (_maxPercent != _minPercent) _maxPercent else _maxPercent + 0.0001f
  val maxPercent: Float = Math.max(__maxPercent, _minPercent)
  val minPercent: Float = Math.min(__maxPercent, _minPercent)

  private[this] val levelIncrease = (maxPercent - minPercent) / Math.max(amount - 1, 0.999f)
  private[this] val levelPercents = minPercent to maxPercent by levelIncrease

  private[this] val decorated = FloatLevelSectionFactory(
    setFactory,
    (if (isLevelIncreasing) levelPercents else levelPercents.reverse): _*)
  override def create(level: Float): Seq[T] = decorated.create(level)
}

case class EmptySectionFactory[T <: ExerciseSet]() extends SectionFactory[T] {
  override def create(level: Float): Seq[T] = Nil
}

case class ModifiedLevelSectionFactory[S <: ExerciseSet](sectionFactory: SectionFactory[S], levelFactor: Float)
  extends SectionFactory[S] {
  override def create(level: Float) = sectionFactory.create(level * levelFactor)
}