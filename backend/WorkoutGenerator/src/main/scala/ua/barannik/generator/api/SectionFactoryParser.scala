package ua.barannik.generator.api

import ua.barannik.common.parsing.ParseNumber.NumberParser
import ua.barannik.common.parsing.{Function, Parse}
import ua.barannik.common.validating.{Fail, ResultValue, ValueOk}
import ua.barannik.generator.entity.ExerciseSet
import ua.barannik.generator.factory._
import ua.barannik.generator.factory.SetFactory

case class SectionFactoryParser[S <: ExerciseSet](setFactory: SetFactory[S]) {

  def parse(input: Function): ResultValue[SectionFactory[S]] = input match {
    case Function("uniform", numberString: String) =>
      for (number <- numberString.asInt(1, 15))
        yield UniformSectionFactory[S](number, setFactory)

    case Function("mountain", minPercentString: String, maxPercentString: String, numberString: String) =>
      for (minPercent <- minPercentString.asFloat(0.01f, 1f);
           maxPercent <- maxPercentString.asFloat(0.01f, 1f);
               number <- numberString.asInt(1, 15))
        yield MountainSectionFactory(minPercent, maxPercent, number, setFactory)

    case Function("float", factors @ _*) => {
      val parsedFactors = factors.view
        .map(_.asInstanceOf[String])
        .map(_.asFloat(0.01f, 1f))

      val parsedListOfParams = parsedFactors
        .foldRight(Parse[List[Float]](Nil)) { (parsedFloat, parsedList) =>
          parsedList.flatMap(list => parsedFloat.map(_ :: list))
        }

      parsedListOfParams.map(list => FloatLevelSectionFactory(setFactory, list: _*))
    }

    case Function("reduced", factorString: String, decorated @ Function(_, _*)) =>
      for ( factor <- factorString.asFloat(0.01f, 10f);
           factory <- parse(decorated))
        yield ModifiedLevelSectionFactory(factory, factor)

    case Function("empty") => ValueOk(EmptySectionFactory())

    case _ => Fail(NoSuchFunction(input))
  }
}
