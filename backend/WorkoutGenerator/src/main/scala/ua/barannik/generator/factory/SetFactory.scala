package ua.barannik.generator.factory

import ua.barannik.generator.entity.{ExerciseSet, Reps, WeightAndReps}

abstract class SetFactory[T <: ExerciseSet] {
  def create(level: Int): T
}

case class RepsFactory() extends SetFactory[Reps] {
  override def create(level: Int): Reps = Reps(Math.max(1, level))
}

case class WeightAndRepsFactory
  (minReps: Int,
   maxReps: Int,
   weightStep: Float,
   minWeight: Float)
  extends SetFactory[WeightAndReps] {
  require(minReps <= maxReps && minReps > 0 && minWeight >= 0 && weightStep >= 0)

  def this(reps: Int, weightStep: Float = 1, minWeight: Float = 0) = this(reps, reps, weightStep, minWeight)

  def create(level: Int): WeightAndReps = {
    require(level >= 0)

    val levelsBetweenRepsBounds: Int = maxReps - minReps + 1
    val weightIncreasingFactor: Int = level / levelsBetweenRepsBounds

    val repsIncrease: Int = level % levelsBetweenRepsBounds
    val weightIncrease: Float = weightIncreasingFactor * weightStep

    WeightAndReps(minReps + repsIncrease, minWeight + weightIncrease)
  }

  def weightIncreaseToLevel(weight: Float): Float = {
    val levelsInOneWeightIncrease = maxReps - minReps + 1
    val neededWeightIncreases = weight / weightStep

    levelsInOneWeightIncrease * neededWeightIncreases
  }

  def levelToWeightIncrease(level: Float): Float = {
    val levelsInOneWeightIncrease = maxReps - minReps + 1
    val weightIncreases = level / levelsInOneWeightIncrease

    weightIncreases * weightStep
  }
}

case class WeightBasedWnRFactory
  (minReps: Int,
   maxReps: Int,
   weightStep: Float,
   minWeight: Float)
  extends SetFactory[WeightAndReps] {

  val decorated = WeightAndRepsFactory(minReps, maxReps, weightStep, minWeight)

  override def create(level: Int) = {
    val Precision = 0.1f
    createWithWeightIncrease(level * Precision)
  }

  def createWithWeightIncrease(weightIncrease: Float) = {
    decorated.create(decorated.weightIncreaseToLevel(weightIncrease).toInt)
  }
}

object WeightAndRepsFactory {
  def apply(reps: Int, weightStep: Float = 1, minWeight: Float = 0) =
    new WeightAndRepsFactory(reps, weightStep, minWeight)
}