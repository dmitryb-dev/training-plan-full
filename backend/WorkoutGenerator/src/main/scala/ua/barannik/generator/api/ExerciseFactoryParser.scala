package ua.barannik.generator.api

import ua.barannik.common.parsing.Parse.IncorrectInput
import ua.barannik.common.parsing.{Function, FunctionParser}
import ua.barannik.generator.entity.{Exercise, ExerciseSet}
import ua.barannik.generator.factory._
import ua.barannik.common.parsing.ParseNumber.NumberParser
import ua.barannik.common.validating.{Fail, ResultValue, ValueOk}
import ua.barannik.generator.factory.{SetFactory, _}

import scala.util.{Failure, Success}

case class ExerciseFactoryParser[S <: ExerciseSet](setFactory: SetFactory[S]) {

  private val EmptyFunction = Function("empty")

  def fromString(input: String): ResultValue[ExerciseFactory[_ <: Exercise[_]]] =
    FunctionParser(input).mainRule.run() match {
      case Success(parsed: Function) => parseFunction(parsed)
      case Failure(_) => Fail(IncorrectInput(input))
    }

  private def parseFunction(input: Function): ResultValue[ExerciseFactory[_ <: Exercise[_]]] = input match {

    case Function("classical", workString: String, warmUpString: String, endString: String) =>
      for (  work <- workString.asInt(1, 15);
           warmUp <- warmUpString.asInt(0, 15);
              end <- endString.asInt(0, 15))
        yield ClassicalExerciseFactory(work, warmUp, end)(setFactory)

    case Function("mountain", minPercent: String, workString: String, endString: String) =>
      for (minPercent <- minPercent.asFloat(0.01f, 1f);
                 work <- workString.asInt(1, 15);
                  end <- endString.asInt(0, 15))
        yield MountainExerciseFactory(minPercent, work, end)(setFactory)

    case Function("custom", workF @ Function(_, _*), warmUpF @ Function(_, _*), endF @ Function(_, _*)) =>
      val sectionFactoryParser = SectionFactoryParser[S](setFactory)
      for (  work <- sectionFactoryParser.parse(workF);
           warmUp <- sectionFactoryParser.parse(warmUpF);
              end <- sectionFactoryParser.parse(endF))
        yield CustomClassicalExerciseFactory(work, warmUp, end)

    case Function("custom", workF @ Function(_, _*), warmUpF @ Function(_, _*)) =>
      parseFunction(Function("custom", workF, warmUpF, EmptyFunction))

    case Function("custom", workF @ Function(_, _*)) =>
      parseFunction(Function("custom", workF, EmptyFunction, EmptyFunction))

    case Function("reduced", factorString: String, decorated @ Function(_, _*)) =>
      for ( factor <- factorString.asFloat(0.01f, 10f);
           factory <- parseFunction(decorated))
        yield ModifiedLevelExerciseFactory(factory, factor)

    case _ => Fail(NoSuchFunction(input))
  }
}
