package ua.barannik.generator.api

import ua.barannik.common.parsing.Function

case class NoSuchFunction(function: Function)