package ua.barannik.generator.entity

class Exercise[+ES <: ExerciseSet](sections: Seq[Section[ES]]) extends Seq[Section[ES]] {
  override def length = sections.length
  override def apply(idx: Int) = sections.apply(idx)
  override def iterator = sections.iterator
}

object Exercise {
  def apply[ES <: ExerciseSet](sections: Seq[Section[ES]]) = new Exercise[ES](sections)
}

case class Sections[+ES <: ExerciseSet](sections: Seq[Section[ES]]) extends Exercise(sections)

case class WorkOnly[+ES <: ExerciseSet](sets: Seq[ES]) extends Exercise(Work(sets) :: Nil)

case class WarmUpWorkEnd[+ES <: ExerciseSet]
  (work: Work[ES],
   warmUp: WarmUp[ES] = WarmUp[ES](Nil),
   end: End[ES] = End[ES](Nil))
  extends Exercise(end :: work :: warmUp :: Nil) {
}