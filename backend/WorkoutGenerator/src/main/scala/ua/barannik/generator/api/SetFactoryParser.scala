package ua.barannik.generator.api

import ua.barannik.common.parsing.FunctionRegex
import ua.barannik.common.parsing.FunctionRegex.InputAny
import ua.barannik.common.parsing.Parse.IncorrectInput
import ua.barannik.common.parsing.ParseNumber.NumberParser
import ua.barannik.common.validating.{Fail, ResultValue, ValueOk}
import ua.barannik.generator.entity.ExerciseSet
import ua.barannik.generator.factory.{RepsFactory, SetFactory, WeightAndRepsFactory, WeightBasedWnRFactory}

object SetFactoryParser {
  val WeightAndRepsPattern = FunctionRegex("weightAndReps", InputAny, InputAny, InputAny, InputAny)
  val WeightBasedWnRPattern = FunctionRegex("weightBasedWnR", InputAny, InputAny, InputAny, InputAny)

  def fromString(input: String): ResultValue[SetFactory[_ <: ExerciseSet]] = input match {
    case "reps" => ValueOk(RepsFactory())

    case WeightAndRepsPattern(minReps, maxReps, weightStep, minWeight) =>
      WithWnRValueOkParams(minReps, maxReps, weightStep, minWeight) create (WeightAndRepsFactory(_, _, _, _))

    case WeightBasedWnRPattern(minReps, maxReps, weightStep, minWeight) =>
      WithWnRValueOkParams(minReps, maxReps, weightStep, minWeight) create WeightBasedWnRFactory

    case _ => Fail(IncorrectInput(input))
  }

  def apply(input: String) = fromString(input)

  private case class WithWnRValueOkParams[T](params: (String, String, String, String)) {
    val (minRepsString, maxRepsString, weightStepString, minWeightString) = params

    def create(factory: (Int, Int, Float, Float) => T): ResultValue[T] =
      for (minReps <- minRepsString.asInt(1, 10000);
           maxReps <- maxRepsString.asInt(1, 10000);
           weightStep <- weightStepString.asFloat(0.001f, 1000f);
           minWeight <- minWeightString.asFloat(0f, 10000f))
        yield factory(
          Math.min(minReps, maxReps),
          Math.max(minReps, maxReps),
          weightStep,
          minWeight
        )
  }
}
