package ua.barannik.generator.api

import ua.barannik.common.parsing.Parse.IncorrectInput
import ua.barannik.common.parsing.ParseNumber.NumberParser
import ua.barannik.common.validating.{Fail, ResultValue}
import ua.barannik.generator.entity.Exercise
import ua.barannik.generator.factory.PlanFactory
import ua.barannik.generator.factory.{ExerciseFactory, PlanFactory}

case class PlanFactoryParser(factory: ExerciseFactory[_ <: Exercise[_]]) {

  def fromString(input: String): ResultValue[Seq[Exercise[_]]] =
    input.split("[-:,]") match {
      case Array(levelString, stepString, numberString) =>
        for (number <- numberString asInt(1, 1000);
              level <- levelString asFloat(0, 10000);
               step <- stepString asFloat(0.01f, 100))
          yield PlanFactory(factory).create(level, step, number)

      case Array(levelString, numberString) =>
        for (number <- numberString asInt(1, 1000);
              level <- levelString asFloat(0, 10000))
          yield PlanFactory(factory).create(level, number)

      case _ => Fail(IncorrectInput(input))
    }
}