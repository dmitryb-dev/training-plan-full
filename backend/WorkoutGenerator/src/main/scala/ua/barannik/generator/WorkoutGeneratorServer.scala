package ua.barannik.generator

import akka.http.scaladsl.Http
import akka.http.scaladsl.coding.Gzip
import akka.http.scaladsl.server.Directives._
import akka.util.Timeout
import ua.barannik.microservice.serialization.ResultJSONMarshaller.typedJackson
import ua.barannik.generator.api.{ExerciseFactoryParser, PlanFactoryParser, SetFactoryParser}
import ua.barannik.microservice.entry.ServerEntry

import scala.concurrent.duration._
import scala.language.postfixOps

object WorkoutGeneratorServer extends ServerEntry {

  implicit val timeout = Timeout(4 seconds)

  val generatorRoute =
    pathPrefix(Segment) { setFactoryDescription =>
      val parsedSetFactory = SetFactoryParser(setFactoryDescription)

      path(IntNumber) { level =>
        complete(parsedSetFactory.map(_.create(level)))
      } ~
      pathPrefix(Segment) { exerciseFactoryDescription =>
        val parsedExerciseFactory = parsedSetFactory
          .flatMap(ExerciseFactoryParser(_).fromString(exerciseFactoryDescription))

        path(DoubleNumber) { level =>
          complete(parsedExerciseFactory.map(_.create(level.floatValue)))
        } ~
        path(Segment) { bounds =>
          complete{
            parsedExerciseFactory
              .map(exerciseFactory => PlanFactoryParser(exerciseFactory))
              .flatMap(planFactory => planFactory.fromString(bounds))
              .map(new GeneratorResult(_))
          }
        }
      }
    }

  val route =
    pathPrefix("generate") {
      pathPrefix("gzip") {
        encodeResponseWith(Gzip) {
          generatorRoute
        }
      } ~
      generatorRoute
    }

  val bindingFuture = Http().bindAndHandle(route, "localhost", 8081)
  info("Workout Generator Server started")
}

class GeneratorResult(list: Seq[_]) {
  val `type`: String = list match {
    case Seq(head, _*) => head.getClass.getSimpleName
    case Nil => "Empty"
  }
  val exercises: Seq[_] = list
}