package ua.barannik.generator.factory

import ua.barannik.generator.entity.SectionConversions._
import ua.barannik.generator.entity._

abstract class ExerciseFactory[+E <: Exercise[_]] {
  def create(level: Float): E
}

case class SectionSeqFactory[S <: ExerciseSet, SF <: SetFactory[_ <: S]]
  (sectionFactories: (SF => Section[S])*)(setFactory: SF)
  extends ExerciseFactory[Exercise[S]] {

  override def create(level: Float): Exercise[S] = Sections(sectionFactories.map(_.apply(setFactory)))
}

case class CustomClassicalExerciseFactory[S <: ExerciseSet]
  (work: SectionFactory[S],
   warmUp: SectionFactory[S] = EmptySectionFactory(),
   end: SectionFactory[S] = EmptySectionFactory())
  extends ExerciseFactory[Exercise[S]] {

  override def create(level: Float): Exercise[S] = (warmUp, end) match {
    case (EmptySectionFactory(), EmptySectionFactory()) => WorkOnly(work.create(level))
    case (_, _) => WarmUpWorkEnd(
      work.create(level),
      warmUp.create(level),
      end.create(level)
    )
  }
}

case class ClassicalExerciseFactory[S <: ExerciseSet]
  (work: Int, warmUp: Int = 0, end: Int = 0)(setFactory: SetFactory[S])
  extends ExerciseFactory[Exercise[S]] {
  require(work > 0 && warmUp >= 0 && end >= 0)

  val MinPercent = 0.5f
  val MaxPercent = 0.9f

  val workFactory = UniformSectionFactory(work, setFactory)

  val warmUpFactory: SectionFactory[S] = warmUp match {
    case 0 => EmptySectionFactory()
    case _ => MountainSectionFactory(MinPercent, MaxPercent, warmUp, setFactory)
  }

  val endFactory: SectionFactory[S] = end match {
    case 0 => EmptySectionFactory()
    case _ => MountainSectionFactory(MaxPercent, MinPercent, end, setFactory)
  }

  private[this] val decoratedExerciseFactory = CustomClassicalExerciseFactory[S](
    workFactory,
    warmUpFactory,
    endFactory
  )

  override def create(level: Float): Exercise[S] = decoratedExerciseFactory.create(level)
}

case class MountainExerciseFactory[S <: ExerciseSet]
  (minPercent: Float, work: Int, end: Int = 0)(setFactory: SetFactory[S])
  extends ExerciseFactory[Exercise[S]] {
  require(minPercent >= 0 && minPercent <= 1 && work > 0)

  override def create(level: Float): Exercise[S] = end match {
    case 0 => WorkOnly(MountainSectionFactory(minPercent, 1, work, setFactory).create(level))
    case _ => WarmUpWorkEnd[S](
      MountainSectionFactory(minPercent, 1, work, setFactory).create(level),
      Nil,
      MountainSectionFactory(1, minPercent, end, setFactory).create(level)
    )
  }
}

case class ModifiedLevelExerciseFactory[E <: Exercise[_]](exerciseFactory: ExerciseFactory[E], levelFactor: Float)
  extends ExerciseFactory[E] {
  override def create(level: Float) = exerciseFactory.create(level * levelFactor)
}