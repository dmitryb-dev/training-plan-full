package ua.barannik.generator.api

import org.scalatest.{FlatSpec, Matchers}
import ua.barannik.common.parsing.Parse.IncorrectInput
import ua.barannik.common.parsing.ParseNumber.NotNumber
import ua.barannik.common.validating.Validate.OutOfBound
import ua.barannik.common.validating.{Fail, ValueOk}
import ua.barannik.generator.factory.{RepsFactory, WeightAndRepsFactory}

class SetPlanFactoryParserTest extends FlatSpec with Matchers {
  it should "Parse reps factory" in {
    SetFactoryParser.fromString("reps") should be(ValueOk(RepsFactory()))
  }
  it should "Parse weight&reps factory" in {
    SetFactoryParser.fromString("weightAndReps(1, 2, 3, 4)") should be(ValueOk(WeightAndRepsFactory(1, 2, 3, 4)))
  }
  it should "Give incorrect input error in case of typo" in {
    SetFactoryParser.fromString("weiAndReps(1, 2, 3, 4)") should be(Fail(IncorrectInput("weiAndReps(1, 2, 3, 4)")))
  }
  it should "Give not number error in case of string param" in {
    SetFactoryParser.fromString("weightAndReps(wr, 2, 3, 4)") should be(Fail(NotNumber("wr")))
  }
  it should "Give not number error in case of string param appears first" in {
    SetFactoryParser.fromString("weightAndReps(wr, 2, -3, 4)") should be(Fail(NotNumber("wr")))
  }
  it should "Give out of bound error in case of wrong number" in {
    SetFactoryParser.fromString("weightAndReps(1, -2, 3, 4)") should be(Fail(OutOfBound(-2, 1, 10000)))
  }
}
