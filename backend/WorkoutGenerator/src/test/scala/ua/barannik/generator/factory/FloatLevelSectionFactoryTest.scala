package ua.barannik.generator.factory

import org.scalatest.{FlatSpec, Matchers}

class FloatLevelSectionFactoryTest extends FlatSpec with Matchers {
  it should "Correctly distribute level between sets" in {
    val factory = FloatLevelSectionFactory[ExerciseSetMock](SetFactoryMock(), 0, 0.1f, 0.2f, 1, 0.5f, 0.3f)

    factory.create(5).map(_.level) should be(0 :: 0 :: 1 :: 5 :: 2 :: 1 :: Nil)
    factory.create(10).map(_.level) should be(0 :: 1 :: 2 :: 10 :: 5 :: 3 :: Nil)
  }
}
