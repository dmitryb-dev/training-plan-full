package ua.barannik.generator.factory

import org.scalatest.{FlatSpec, Matchers}
import ua.barannik.generator.entity.Declarative.weightUnitsFloat
import ua.barannik.generator.entity.WeightAndReps

import scala.language.postfixOps

class WeightAndRepsPlanFactoryTest extends FlatSpec with Matchers {
  it should "Generate correct weights in case of no reps bounds" in {
    val factory = WeightAndRepsFactory(7, 1.5f, 2.5f)

    factory.create(0) should be(WeightAndReps(7, 2.5 kg))
    factory.create(1) should be(WeightAndReps(7, 4 kg))
    factory.create(2) should be(WeightAndReps(7, 5.5 kg))
  }
  it should "Increase reps firstly and weight after" in {
    val factory = WeightAndRepsFactory(5, 8, 1.5f, 2.5f)

    factory.create(0) should be(WeightAndReps(5, 2.5 kg))
    factory.create(1) should be(WeightAndReps(6, 2.5 kg))
    factory.create(2) should be(WeightAndReps(7, 2.5 kg))
    factory.create(3) should be(WeightAndReps(8, 2.5 kg))

    factory.create(4) should be(WeightAndReps(5, 4 kg))
    factory.create(5) should be(WeightAndReps(6, 4 kg))
    factory.create(6) should be(WeightAndReps(7, 4 kg))
    factory.create(7) should be(WeightAndReps(8, 4 kg))

    factory.create(8) should be(WeightAndReps(5, 5.5 kg))
    factory.create(9) should be(WeightAndReps(6, 5.5 kg))
  }

  it should "Correctly works with weight" in {
    val factory = WeightBasedWnRFactory(5, 7, 3, 21f)

    factory.create(780) should be(WeightAndReps(5, 99 kg))
    factory.create(790) should be(WeightAndReps(6, 99 kg))
    factory.create(800) should be(WeightAndReps(7, 99 kg))
    factory.create(810) should be(WeightAndReps(5, 102 kg))
  }
}
