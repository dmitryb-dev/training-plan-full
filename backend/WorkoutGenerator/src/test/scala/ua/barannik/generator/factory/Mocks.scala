package ua.barannik.generator.factory

import ua.barannik.generator.entity.{Exercise, ExerciseSet}

case class ExerciseSetMock(level: Int) extends ExerciseSet
case class SetFactoryMock() extends SetFactory[ExerciseSetMock] {
  override def create(level: Int): ExerciseSetMock = ExerciseSetMock(level)
}
case class ExerciseMock() extends Exercise(Nil)
case class ExerciseFactoryMock() extends ExerciseFactory[ExerciseMock] {
  override def create(level: Float) = ???
}