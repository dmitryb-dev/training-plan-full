package ua.barannik.generator.factory

import org.scalatest.{FlatSpec, Matchers}

class MountainSectionFactoryTest extends FlatSpec with Matchers {
  it should "Correctly work in case min ~= max" in {
    val factory = MountainSectionFactory[ExerciseSetMock](0.5f, 0.5000001f, 3, SetFactoryMock())
    factory.create(10).map(_.level) should be(5 :: 5 :: 5 :: Nil)
  }
  it should "Correctly work in case min = max" in {
    val factory = MountainSectionFactory[ExerciseSetMock](0.5f, 0.5f, 3, SetFactoryMock())
    factory.create(10).map(_.level) should be(5 :: 5 :: 5 :: Nil)
  }

  it should "Correctly work with 1 set" in {
    val factory = MountainSectionFactory[ExerciseSetMock](0.6f, 1, 1, SetFactoryMock())
    factory.create(10).map(_.level) should be(6 :: Nil)
  }
  it should "Correctly work with 1 set and decreasing level" in {
    val factory = MountainSectionFactory[ExerciseSetMock](1, 0.6f, 1, SetFactoryMock())
    factory.create(10).map(_.level) should be(6 :: Nil)
  }
  it should "Correctly work with 2 sets" in {
    val factory = MountainSectionFactory[ExerciseSetMock](0.5f, 1, 2, SetFactoryMock())
    factory.create(10).map(_.level) should be(5 :: 10 :: Nil)
  }
  it should "Correctly distribute increasing level between sets" in {
    val factory = MountainSectionFactory[ExerciseSetMock](0.6f, 1, 5, SetFactoryMock())

    factory.create(10).map(_.level) should be(6 :: 7 :: 8 :: 9 :: 10 :: Nil)
    factory.create(5).map(_.level) should be(3 :: 3 :: 4 :: 4 :: 5 :: Nil)
  }

  it should "Correctly distribute decreasing level between sets" in {
    val factory = MountainSectionFactory[ExerciseSetMock](1, 0.2f, 5, SetFactoryMock())

    factory.create(10).map(_.level) should be(10 :: 8 :: 6 :: 4 :: 2 :: Nil)
    factory.create(5).map(_.level) should be(5 :: 4 :: 3 :: 2 :: 1 :: Nil)
  }
}
