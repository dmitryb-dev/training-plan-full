package ua.barannik.generator.factory

import org.scalatest.{FlatSpec, Matchers}

class UniformSectionPlanFactoryTest extends FlatSpec with Matchers {
  it should "Correctly distribute level between sets" in {
    val factory = UniformSectionFactory(3, SetFactoryMock())

    factory.create(7).map(_.level) should be(7 :: 7 :: 7 :: Nil)
    factory.create(7.2f).map(_.level) should be(7 :: 7 :: 7 :: Nil)
    factory.create(7.4f).map(_.level) should be(8 :: 7 :: 7 :: Nil)
    factory.create(7.7f).map(_.level) should be(8 :: 8 :: 7 :: Nil)
    factory.create(7.9f).map(_.level) should be(8 :: 8 :: 7 :: Nil)
    factory.create(8).map(_.level) should be(8 :: 8 :: 8 :: Nil)
  }
}
