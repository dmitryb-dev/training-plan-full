package ua.barannik.persistance.dao

import scala.collection.mutable

case class LocalCrud[E, ID](idGenerator: E => ID) extends Crud[E, ID] {
  private val store = new mutable.HashMap[ID, E]

  override def create(entity: E): ID = {
    val id = idGenerator(entity)
    store.put(id, entity)
    id
  }

  override def remove(id: ID) = store.remove(id)

  override def update(id: ID, entity: E) = store.put(id, entity)

  override def find(id: ID): Option[E] = store.get(id)
}
