package ua.barannik.persistance.entity

trait Named {
  val name: String
}
