package ua.barannik.persistance.mongo

import ua.uraty.utils.mongo.MongoId

import scala.concurrent.{ExecutionContext, Future}

object MongoIdGenerator {

  private val generator = MongoId(Mongo.routineDB)

  def generateId(entity: String)
                (implicit context: ExecutionContext): Future[Long] =
    generator.generateId(entity)(context)

  def withIdFor[T](counter: String)
                  (idConsumer: Long => Future[T])
                  (implicit context: ExecutionContext): Future[T] =
    generator.withIdFor(counter)(idConsumer)(context)
}