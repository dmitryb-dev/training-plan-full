package ua.barannik.persistance.dao

import ua.barannik.common.validating.{Result, ResultValue}

import scala.concurrent.Future

trait DaoService[ENTITY, ID] {
  type FutureValue[T] = Future[ResultValue[T]]
  type FutureResult = Future[Result]

  def findAll(): FutureValue[Seq[ENTITY]]
  def find(id: ID): FutureValue[ENTITY]
  def create(entity: ENTITY): FutureValue[ID]
  def update(id: ID, entity: ENTITY): FutureResult
  def remove(id: ID): FutureResult
}
