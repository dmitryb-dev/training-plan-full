package ua.barannik.persistance.entity

trait UserProperty {
  type UserName = String

  val owner: UserName
}
