package ua.barannik.persistance.mongo

import org.mongodb.scala.bson.BsonArray
import org.mongodb.scala.bson.collection.immutable.Document

import scala.collection.JavaConverters._

object Parsers {
  implicit class BsonArrayParser(bsonArray: BsonArray) {
    def parseDocuments[T](mapper: Document => T) = bsonArray.iterator().asScala
      .map(_.asDocument())
      .map(new Document(_))
      .map(mapper(_))
      .toList
  }
}
