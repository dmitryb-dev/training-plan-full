package ua.barannik.persistance.dao

import scala.concurrent.Future

trait ImmutableStore[ENTITY, ID] {
  def create(entity: ENTITY): ID
  def remove(id: ID)
  def find(id: ID): Option[ENTITY]
}

trait Crud[ENTITY, ID] extends ImmutableStore[ENTITY, ID] {
  def update(id: ID, entity: ENTITY)
}

trait FutureCrud[ENTITY, ID] {
  def create(entity: ENTITY): Future[ID]
  def remove(id: ID): Future[ID]
  def update(id: ID, entity: ENTITY): Future[ID]
  def find(id: ID): Future[Option[ENTITY]]
}

trait FindAllFuture[ENTITY] {
  def findAll(): Future[Seq[ENTITY]]
}

object CrudConversion {
  def crudToFutureCrud[E, ID](crud: Crud[E, ID]): FutureCrud[E, ID] = {
    new FutureCrud[E, ID] {
      override def create(entity: E): Future[ID] = Future.successful(crud.create(entity))

      override def remove(id: ID) = Future.successful {
        crud.remove(id)
        id
      }

      override def update(id: ID, entity: E) = Future.successful {
        update(id, entity)
        id
      }

      override def find(id: ID): Future[Option[E]] = Future.successful(crud.find(id))
    }
  }
}
