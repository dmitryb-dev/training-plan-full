package ua.barannik.persistance.entity

trait Id[I] {
  val id: I
}

trait LongId extends Id[Long]