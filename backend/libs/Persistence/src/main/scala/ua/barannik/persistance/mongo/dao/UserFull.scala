package ua.barannik.persistance.mongo.dao

import salat.annotations._

case class UserFull(@Key("_id") login: String, pass: String, workout: List[Workout])

case class Workout(@Key("_id") id: Long, name: String, days: List[Day])

case class Day(@Key("_id") id: Long, name: String, exercises: List[Exercise])

case class Exercise(@Key("_id") id: Long, name: String)