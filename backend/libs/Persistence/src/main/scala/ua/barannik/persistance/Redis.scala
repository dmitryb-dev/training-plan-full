package ua.barannik.persistance

import com.redis.{RedisClient, RedisClientPool}

trait RedisRequestPerformer {
  def request[T](request: RedisClient => T): T
}

object Redis {
  val DefaultClients = new RedisClientPool("localhost", 6379, secret = Some("redis"))

  def apply(): RedisRequestPerformer = new RedisRequestPerformer {
    override def request[T](request: RedisClient => T): T = DefaultClients.withClient(request)
  }
}
