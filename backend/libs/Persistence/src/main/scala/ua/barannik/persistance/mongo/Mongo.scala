package ua.barannik.persistance.mongo

import org.mongodb.scala.MongoClient


object Mongo {
  object RoutineDB {
    def apply() = MongoClient().getDatabase("routines")

    val Users = Mongo.RoutineDB().getCollection("users")
    val Workouts = Mongo.RoutineDB().getCollection("workouts")
    val Days = Mongo.RoutineDB().getCollection("days")
    val Exercises = Mongo.RoutineDB().getCollection("exercises")
  }

  object HistoryDB {
    def apply() = MongoClient().getDatabase("history")
  }
}

object Counters {
  val Users = "users"
  val Workouts = "workouts"
}