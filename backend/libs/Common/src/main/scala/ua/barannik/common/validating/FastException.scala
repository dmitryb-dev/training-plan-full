package ua.barannik.common.validating

import java.lang

class FastException(fillStackTrace: Boolean) extends RuntimeException() {
  def this() = this(FastException.isStackTraceEnabled)

  override def fillInStackTrace(): Throwable = {
    if (fillStackTrace)
      super.fillInStackTrace()
    this
  }
}

object FastException {
  val isStackTraceEnabled: Boolean =
    Option(System.getProperty("stackTraceEnabled")).exists(new lang.Boolean(_).booleanValue())
}
