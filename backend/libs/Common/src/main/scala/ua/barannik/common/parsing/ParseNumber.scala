package ua.barannik.common.parsing

import ua.barannik.common.validating.{ResultValue, Validate}
import ua.barannik.common.validating.Validate.Number

object ParseNumber {
  case class NotNumber(input: String)

  def apply(input: String) = Parse[String, Number, NotNumber](input, input => input.toDouble, input => NotNumber(input))

  def apply(input: String, min: Number, max: Number): ResultValue[Number] =
    ParseNumber(input)
      .flatMap(value => Validate(value, min, max))

  implicit class NumberParser(input: String) {
    def asInt(min: Int, max: Int): ResultValue[Int] =
      ParseNumber(input, min, max)
        .map(_.intValue())

    def asFloat(min: Float, max: Float): ResultValue[Float] =
      ParseNumber(input, min, max)
        .map(_.floatValue())
  }
}
