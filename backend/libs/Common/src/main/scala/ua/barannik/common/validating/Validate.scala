package ua.barannik.common.validating

object Validate {
  type Validate[T] = ResultValue[T]
  type Valid[T] = ValueOk[T]

  def apply[T, E](input: T)(validator: T => Boolean, error: => E): Validate[T] =
    if (validator(input))
      ValueOk[T](input)
    else
      Fail(error)

  type Number = Double
  case class OutOfBound(actual: Number, min: Number, max: Number)

  def apply(input: Number, min: Number, max: Number): Validate[Number] = {
    Validate(input)(input => input >= min && input <= max, OutOfBound(input, min, max))
  }
}