package ua.barannik.common.parsing

import ua.barannik.common.validating.{Fail, ValueOk, ResultValue}

import scala.util.{Failure, Success, Try}

object Parse {
  case class IncorrectInput[T](input: T)

  def apply[T](parsed: => T): ResultValue[T] = Try(parsed) match {
    case Success(value) => ValueOk(value)
    case Failure(e) => Fail(e)
  }

  def apply[A, B, E](input: A, parser: A => B, error: A => E): ResultValue[B] = Try(parser(input)) match {
    case Success(value) => ValueOk(value)
    case Failure(_) => Fail(error(input))
  }

  def apply[A, B](input: A)(parser: A => B): ResultValue[B] =
    Parse[A, B, IncorrectInput[A]](input, parser, input => IncorrectInput(input))
}
