package ua.barannik.common.parsing

import org.parboiled2._

case class FunctionParser(input: ParserInput) extends Parser {

  def mainRule = rule { function ~ EOI }

  def function = rule { capture(oneOrMore(CharPredicate.AlphaNum)) ~ '(' ~ params ~ ')' ~> (Function(_, _: _*)) }

  def params = rule { zeroOrMore(param ~ ws).separatedBy(',' ~ ws) ~> (Array(_:_*)) }

  def param: Rule1[Any] = rule { function | capture(symbol) }

  def symbol = rule { oneOrMore(CharPredicate.AlphaNum | '.') }

  def ws = rule { zeroOrMore(' ') }
}

case class Function(name: String, params: Any*)
