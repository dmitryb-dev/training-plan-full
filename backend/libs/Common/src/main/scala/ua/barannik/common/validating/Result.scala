package ua.barannik.common.validating

trait Result
trait ResultOk extends Result

trait ResultValue[+A] extends Result {
  val content: Any
  def map[B](mapper: A => B): ResultValue[B]
  def flatMap[B](mapper: A => ResultValue[B]): ResultValue[B]
  def foreach(consumer: A => Any)
}

object ResultValue {
  case class NoSuchElement()
  implicit def fromOption[T, E](option: Option[T], error: E = NoSuchElement) =
    option
      .map(ValueOk(_))
      .getOrElse(Fail(error))
}

case class Ok() extends ResultOk

case class ValueOk[+T](value: T) extends ResultValue[T] with ResultOk {
  override val content: T = value

  override def map[B](mapper: T => B) = ValueOk(mapper(value))

  override def flatMap[B](mapper: T => ResultValue[B]) = mapper(value)

  override def foreach(consumer: T => Any) = consumer(value)
}

case class Fail[E](error: E) extends ResultValue[Nothing] {
  override val content: E = error

  override def map[B](mapper: Nothing => B) = this

  override def flatMap[B](mapper: Nothing => ResultValue[B]) = this

  override def foreach(consumer: Nothing => Any) = {}
}