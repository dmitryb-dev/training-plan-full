package ua.barannik.common.transform

import scala.concurrent.{ExecutionContext, Future}
import scala.language.implicitConversions

case class FO[OPT_VALUE](value: Future[Option[OPT_VALUE]]) {
  implicit val context = ExecutionContext.global

  def map[R](mapper: OPT_VALUE => R): FO[R] =
    FO(value.map(option => option.map(mapper)))

  def flatMap[R](mapper: OPT_VALUE => FO[R]): FO[R] =
    FO(value.flatMap {
      case Some(wrapped) => mapper(wrapped).value
      case None => Future.successful(None)
    })
}

object FO {
  type FutureOption[T] = FO[T]

  implicit def futureOptionConverter[T](futureOption: Future[Option[T]]): FO[T] =
    FO(futureOption)

  implicit def futureOptionConverter[T](option: Option[T]): FO[T] =
    FO(Future.successful(option))

  implicit def futureOptionConverter[T](futureOption: FO[T]): Future[Option[T]] =
    futureOption.value
}


