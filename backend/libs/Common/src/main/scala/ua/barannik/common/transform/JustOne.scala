package ua.barannik.common.transform

import scala.language.implicitConversions

object JustOne {
  implicit def requireOne[T](iterable: java.lang.Iterable[T]) = {
    val iter = iterable.iterator
    if (iter.hasNext) {
      Some(iter.next())
    } else {
      None
    }
  }
}
