package ua.barannik.common.parsing

/**
  * It creates regex for parsing inputs as "functionName(p, p, p)"
  */
object FunctionRegex {
  private val paramsSeparator = ",\\s*"
  private val ignoreCase = "(?i)"

  type InputType = String
  val InputInt: InputType = """(\-?\d+)""""
  val InputFloat: InputType = """(\-?\d+\.?\d*)"""
  val InputAny: InputType = """(\-?[\w\.]+)"""

  def createParamsRegexString(params: InputType*): String =
    if (params.isEmpty)
      """(\(\))?"""
    else
      params.mkString("\\(", paramsSeparator, "\\)")

  def apply(name: String, params: InputType*) =
    (ignoreCase + name + createParamsRegexString(params: _*)).r
}
