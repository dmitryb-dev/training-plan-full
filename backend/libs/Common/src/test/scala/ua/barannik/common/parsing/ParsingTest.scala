package ua.barannik.common.parsing

import org.scalatest.{FlatSpec, Matchers}
import ua.barannik.common.parsing.ParseNumber.{NotNumber, NumberParser}
import ua.barannik.common.validating.Validate.OutOfBound
import ua.barannik.common.validating.{Fail, ValueOk}

class ParsingTest extends FlatSpec with Matchers {
  it should "Parse int" in {
    "7.12".asInt(0, 10) should be(ValueOk(7))
  }
  it should "Parse float with dot" in {
    "7.1".asFloat(0, 10) should be(ValueOk(7.1f))
  }
  it should "Parse float" in {
    "7".asFloat(0, 10) should be(ValueOk(7))
  }
  it should "Fail with out of bounds" in {
    "7".asFloat(0, 5) should be(Fail(OutOfBound(7, 0, 5)))
  }
  it should "Fail with parsing error" in {
    "s7".asFloat(0, 5) should be(Fail(NotNumber("s7")))
  }
  it should "Correctly works with for expression" in {
    val res =
      for (v1 <- "1".asFloat(0, 3);
           v2 <- "2".asFloat(0, 3);
           v3 <- "3".asFloat(0, 3))
        yield v1 + v2 + v3
    res should be(ValueOk(6))
  }
  it should "Fail at first wrong" in {
    val nonvalid =
      for (v1 <- "1".asFloat(0, 3);
           v2 <- "7".asFloat(0, 5);
           v3 <- "s3".asFloat(0, 3))
        yield v1 + v2 + v3
    nonvalid should be(Fail(OutOfBound(7, 0, 5)))

    val parsingError =
      for (v1 <- "1".asFloat(0, 3);
           v3 <- "s3".asFloat(0, 3);
           v2 <- "7".asFloat(0, 5))
        yield v1 + v2 + v3
    parsingError should be(Fail(NotNumber("s3")))
  }
}
