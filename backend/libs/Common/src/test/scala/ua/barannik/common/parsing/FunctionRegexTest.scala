package ua.barannik.common.parsing

import java.util.regex.Pattern

import org.scalatest.{FlatSpec, Matchers}
import ua.barannik.common.parsing.FunctionRegex.{InputFloat, InputAny}

class FunctionRegexTest extends FlatSpec with Matchers {
  it should "Correctly work with function in any case" in {
    implicit val pattern = FunctionRegex("func").pattern
    test("func()")
    test("FUNC()")
    test("Func()")
    test("fUNc()")
  }

  it should "Correctly work with functions with no parameters" in {
    implicit val pattern = FunctionRegex("func").pattern
    test("func()")
    test("func")
  }

  it should "Correctly parse function with float parameters" in {
    val regex = FunctionRegex("func", InputFloat, InputFloat)
    implicit val pattern = regex.pattern
    test("func(1, 2)")
    test("func(1.,2)")
    test("func(1.,2.)")
    test("func(1,2.)")
    test("func(1.3,2)")
    test("func(1,2.3)")
    test("func(1.3,2.4)")
    test("func(1, 2)")
    test("func(1., 2)")
    test("func(1.3, 2)")

    "func(1, 2)" match { case regex("1", "2") => }
    "func(1., 2.)" match { case regex("1.", "2.") => }
    "func(1.3, 2.4)" match { case regex("1.3", "2.4") => }
    "func(1., 2.3)" match { case regex("1.", "2.3") => }
  }

  it should "Correctly parse any val" in {
    val regex = FunctionRegex("func", InputAny, InputAny)
    implicit val pattern = regex.pattern
    test("func(1, wr)")
    test("func(1, wr.)")
    test("func(1.we, wr)")
    test("func(.1, w.r)")

    "func(1.rs, r2)" match { case regex("1.rs", "r2") => }
    "func(s1., 2)" match { case regex("s1.", "2") => }
    "func(b, a)" match { case regex("b", "a") => }
    "func(1, 2)" match { case regex("1", "2") => }
  }

  def test(value: String)(implicit pattern: Pattern) =
    assert(pattern.matcher(value).matches())
}
