package ua.barannik.common.parsing

import org.scalatest.{FlatSpec, Matchers}

import scala.util.Success

class FunctionParserTest extends FlatSpec with Matchers {

  it should "Correctly parse empty function" in {
    FunctionParser("func1()").mainRule.run() match {
      case Success(Function("func1")) =>
    }
  }

  it should "Correctly parse function with parameters" in {
    FunctionParser("func1(1,2,3)").mainRule.run() match {
      case Success(Function("func1", "1", "2", "3")) =>
    }
  }

  it should "Correctly parse functions inside function" in {
    FunctionParser("func1(1,fun2(),2)").mainRule.run() match {
      case Success(Function("func1", "1", Function("fun2"), "2")) =>
    }
  }

}
