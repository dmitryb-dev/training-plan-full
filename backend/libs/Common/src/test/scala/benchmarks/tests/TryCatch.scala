package benchmarks.tests

import benchmarks.BenchmarkBuilder
import ua.barannik.common.validating
import ua.barannik.common.validating.{FastException, Result, ResultValue, ValueOk}

import scala.util.Random

object Funcs {
  def function() = Random.nextLong() * Random.nextDouble()
  def next() = Random.nextLong() * Random.nextDouble()
  def handler() = Random.nextInt() * Random.nextFloat()
  def exception(exception: Throwable) = {
    function()
    throw exception
  }
  def recursive(depth: Int)(function: => Any): Any = {
    if (depth > 0) {
      recursive(depth -1)(function)
    } else {
      function
    }
    depth
  }
  def recursiveResultFail(depth: Int)(function: => Any): ResultValue[Int] = {
    if (depth > 0) {
      recursiveResultFail(depth - 1)(function).map(value => value + 1)
    } else {
      function
      validating.Fail(1)
    }
  }
  def recursiveResultFailMatch(depth: Int)(function: => Any): ResultValue[Int] = {
    if (depth > 0) {
      recursiveResultFail(depth - 1)(function) match {
        case ValueOk(v) => ValueOk(v + 1)
        case e @ validating.Fail(_) =>
          handler()
          e
      }
    } else {
      function
      validating.Fail(1)
    }
  }

  def warmUp() = (1 to 10000000).foreach { _ =>
    function()
    next()
    handler()
  }
}

import benchmarks.tests.Funcs._

object Ok extends App {
  warmUp()
  val runner = BenchmarkBuilder(1000000, 1000000)
  runner.benchmark("Pure") {
    function()
    next()
  }
  runner.benchmark("If") {
    if (function() * 0 < 1) {
      next()
    } else {
      handler()
    }
  }
  runner.benchmark("TryCatch") {
    try {
      function()
      next()
    } catch {
      case _: Exception => handler()
    }
  }
  runner.benchmark("Result map") {
    ValueOk(function()).map(_ => next())
  }
  runner.benchmark("Result match") {
    ValueOk(function()).asInstanceOf[ResultValue[_]] match {
      case ValueOk(_) => next()
      case validating.Fail(_) =>
    }
  }
  runner.run()
}

object Fail extends App {
  warmUp()
  val runner = BenchmarkBuilder(1000000, 1000000)
  runner.benchmark("Pure") {
    function()
    handler()
  }
  runner.benchmark("If") {
    if (function() * 0 > 1) {
      next()
    } else {
      handler()
    }
  }
  runner.benchmark("TryCatch") {
    try {
      function()
      throw new Exception()
      next()
    } catch {
      case _: Exception => handler()
    }
  }
  runner.benchmark("TryCatch fastException without stacktrace") {
    try {
      exception(new FastException())
      next()
    } catch {
      case _: Exception => handler()
    }
  }
  runner.benchmark("TryCatch fastException with stacktrace") {
    try {
      function()
      throw new FastException(true)
      next()
    } catch {
      case _: Exception => handler()
    }
  }
  runner.benchmark("Result map") {
    val res: ResultValue[Any] = validating.Fail(function())
    res.map(_ => next())
    res match {
      case ValueOk(_) => next()
      case validating.Fail(_) => handler()
    }
  }
  runner.benchmark("Result without map") {
    val res: ResultValue[Any] = validating.Fail(function())
    res match {
      case ValueOk(_) => next()
      case validating.Fail(_) => handler()
    }
  }
  runner.benchmark("Result match") {
    validating.Fail(function()).asInstanceOf[ResultValue[_]] match {
      case validating.Fail(_) => handler()
      case ValueOk(_) =>
    }
  }
  runner.run()
}

object Resurcive extends App {
  val REC = 10
  val runner = BenchmarkBuilder(100000, 100000)

  runner.benchmark("Pure") {
    recursive(REC) {
      function()
    }
  }
  runner.benchmark("Result map") {
    recursiveResultFail(REC)(function())
  }
  runner.benchmark("Result match") {
    recursiveResultFailMatch(REC)(function())
  }
  runner.benchmark("Exception without stacktrace") {
    try {
      recursive(REC) {
        function()
        exception(new FastException())
      }
    } catch {
      case _: Exception =>
    }
  }
  runner.benchmark("Exception with stacktrace") {
    try {
      recursive(REC) {
        function()
        exception(new FastException(true))
      }
    } catch {
      case _: Exception =>
    }
  }
  runner.run()
}

