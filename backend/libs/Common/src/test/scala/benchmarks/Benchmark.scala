package benchmarks

case class Benchmark(tries: Int, warmUp: Int, tests: (String, () => Any)*) {
  def run() = {
    for (i <- 1 to warmUp;
         wuTest <- tests.reverse)
      wuTest._2()

    val times = tests
      .map { case (name, func) =>
        (name, runOne(func))
      }
    val minTime = times.map { case (_, time) => time }.min.floatValue()
    times.foreach { case (name, time) =>
      println(f"$name: ${time}ms, ${time / Math.max(minTime, 1) * 100 - 100}%1.1f%% slower")
    }
  }

  private def runOne(test: () => Any): Long = {
    val startTime = System.currentTimeMillis()
    (1 to tries).foreach(_ => test())
    System.currentTimeMillis() - startTime
  }
}

case class BenchmarkBuilder(tries: Int, warmUp: Int) {
  var functions: Seq[(String, () => Any)] = Nil

  def benchmark(name: String)(toTest: => Any) =
    functions = functions :+ (name, () => toTest)

  def run() = Benchmark(tries, warmUp, functions: _*).run()
}