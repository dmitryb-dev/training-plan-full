package ua.barannik.microservice.serialization

import akka.http.scaladsl.marshalling.{Marshaller, ToEntityMarshaller}
import akka.http.scaladsl.model.{ContentTypes, HttpEntity}
import com.jsoniter.output.{EncodingMode, JsonStream}
import com.jsoniter.spi._
import scala.collection.JavaConverters._

object Jsoniter {

  JsonStream.setMode(EncodingMode.DYNAMIC_MODE)
  JsoniterSpi.registerExtension(ScalaFieldExtension())
  JsoniterSpi.registerTypeEncoder(classOf[Seq[_]], IterableEncoder())

  def json[T]: ToEntityMarshaller[T] =
    Marshaller.withFixedContentType(ContentTypes.`application/json`) { value =>
      HttpEntity(JsonStream.serialize(value))
    }

  implicit val makeAllJson: ToEntityMarshaller[Any] = json[Any]

  implicit class JSONSerializable(obj: Any) {
    def json: String = JsonStream.serialize(obj)
  }

  case class IterableEncoder() extends Encoder {
    override def encode(obj: scala.Any, stream: JsonStream) = {
      stream.writeVal(asJavaCollection(obj.asInstanceOf[Iterable[_]]))
    }
  }

  case class ScalaFieldExtension() extends EmptyExtension {
    override def updateClassDescriptor(desc: ClassDescriptor): Unit = {
      val thisMethods = desc.clazz.getDeclaredMethods
      val thisFieldNames = desc.clazz.getDeclaredFields.map(_.getName)
      val publicMethods = desc.clazz.getMethods
      val suitableMethods = thisMethods.intersect(publicMethods)
        .filterNot(_.getReturnType == Void.TYPE)
        .filter(_.getParameterCount == 0)
        .filter(m => thisFieldNames.contains(m.getName))
      desc.getters = seqAsJavaList(suitableMethods
        .map { m =>
          val binding = new Binding(desc.classInfo, desc.lookup, m.getReturnType)
          binding.name = m.getName
          binding.toNames = Array(m.getName)
          binding.method = m
          binding
        }
      )
    }
  }
}
