package ua.barannik.microservice.serialization

import akka.http.scaladsl.marshalling.{Marshaller, ToEntityMarshaller}
import akka.http.scaladsl.model.{ContentTypes, HttpEntity}
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.scala.DefaultScalaModule

object Jackson {
  val mapper = new ObjectMapper()
  mapper.registerModule(DefaultScalaModule)

  def json[T]: ToEntityMarshaller[T] =
    Marshaller.withFixedContentType(ContentTypes.`application/json`) { value =>
      HttpEntity(mapper.writeValueAsString(value))
    }

  implicit val makeAllJson: ToEntityMarshaller[Any] = json[Any]

  implicit class JSONSerializable(obj: Any) {
    def json: String = mapper.writeValueAsString(obj)
  }
}
