package ua.barannik.microservice.entry

import akka.actor.ActorSystem
import akka.stream.ActorMaterializer
import ua.uraty.logging.scala.Logging

import scala.concurrent.ExecutionContextExecutor

trait ContainsActorSystem {
  implicit val system: ActorSystem = ActorSystem(Names.ActorSystemName)
}

trait ActorsEntry extends App with ContainsActorSystem

trait ServerEntry extends ActorsEntry with Logging {
  implicit val materializer: ActorMaterializer = ActorMaterializer()
  implicit val executionContext: ExecutionContextExecutor  = system.dispatcher
}
