package ua.barannik.microservice.serialization

import akka.http.scaladsl.marshalling.{Marshaller, ToEntityMarshaller}
import akka.http.scaladsl.model.{ContentTypes, HttpEntity}
import akka.stream.scaladsl.Source
import akka.util.ByteString
import ua.barannik.common.validating.{Ok, Result, ResultValue}

object ResultJSONMarshaller {
  implicit val typedJackson: ToEntityMarshaller[Result] = typed(Jackson.mapper.writeValueAsString)
  implicit val typedJsoniter: ToEntityMarshaller[Result] = typed(Jsoniter.JSONSerializable(_).json)

  def typed(serializer: Any => String): ToEntityMarshaller[Result] = Marshaller.withFixedContentType(ContentTypes.`application/json`) {
    case parsed: ResultValue[_] =>
      HttpEntity(ContentTypes.`text/plain(UTF-8)`, Source {
        ("{\"" :: parsed.content.getClass.getSimpleName :: "\": " :: serializer(parsed.content) :: "}" :: Nil)
          .map(ByteString(_))
      })
    case Ok() => HttpEntity(ContentTypes.`text/plain(UTF-8)`, "Done")
  }
}
