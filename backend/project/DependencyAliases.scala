import sbt._

object DependencyAliases {

  val scalatest = Seq("org.scalatest" % "scalatest_2.12" % "3.0.1" % "test")

  val jackson = Seq("com.fasterxml.jackson.module" % "jackson-module-scala_2.12" % "2.9.2")
  val jsoniter = Seq("com.jsoniter" % "jsoniter" % "0.9.18")

  val javassist = Seq("org.javassist" % "javassist" % "3.21.0-GA")

  val parboiled = Seq("org.parboiled" %% "parboiled" % "2.1.4")

  val logging = Seq(
    "com.typesafe.scala-logging" %% "scala-logging" % "3.7.2",
    "ch.qos.logback" % "logback-classic" % "1.2.3",
    "ua.uraty.utils" % "logging-scala" % "1.0"
  )

  val akka = Seq(
    "com.typesafe.akka" %% "akka-actor" % "2.5.6",
    "com.typesafe.akka" %% "akka-stream" % "2.5.6",
    "com.typesafe.akka" %% "akka-testkit" % "2.5.6" % Test
  )
  val akkaHttp = Seq("com.typesafe.akka" %% "akka-http" % "10.0.10")

  val bcrypt = Seq("org.mindrot" % "jbcrypt" % "0.4")

  val redis = Seq("net.debasishg" %% "redisclient" % "3.4")

  val mongo =  Seq(
    "org.mongodb.scala" %% "mongo-scala-driver" % "2.2.0",
    "com.github.salat" %% "salat" % "1.11.2",
    "ua.uraty.utils.mongo" % "id" % "1.0"
  )

}
