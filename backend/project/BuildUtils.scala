import sbt.Keys._
import sbt.ModuleID

object BuildUtils {
  def dependsOn(dependencies: Seq[ModuleID]*) = Seq(libraryDependencies ++= dependencies.reduce(_ ++ _))
  def projectSettings(info: Seq[sbt.Def.Setting[_]], dependencies: Seq[ModuleID]*) = info ++ dependsOn(dependencies: _*)

  case class SettingsFactory(info: Seq[sbt.Def.Setting[_]]) {
    def withDependencies(dependencies: Seq[ModuleID]*) = projectSettings(info, dependencies: _*)
  }
}
