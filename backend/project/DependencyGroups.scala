import DependencyAliases._

object DependencyGroups {

  val testing = scalatest
  val serializing = jackson ++ jsoniter ++ javassist
  val api = DependencyAliases.akkaHttp ++ DependencyAliases.akka ++ serializing


  val Base = testing ++ DependencyAliases.logging


  val Common = Base ++ parboiled

  val MicroService = Base ++ api

  val Persistence = Base ++ DependencyAliases.redis ++ DependencyAliases.mongo


  val AuthService = DependencyAliases.bcrypt

  val WebApp = Base

}
