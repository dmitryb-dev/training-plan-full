import sbt.Keys._
import sbt._

object Settings {
  val Info = Seq(
    organization := "ua.barannik",
    version := "0.1.0-SNAPSHOT",
    scalaVersion := "2.12.2",
    scalacOptions ++= Seq("-encoding", "UTF-8"),
    resolvers += "wagon.git" at "https://api.bitbucket.org/1.0/repositories/Uraty/mvn/raw/releases/"
  )

  val projectSettingsFactory = BuildUtils.SettingsFactory(Info)

  val Common = projectSettingsFactory.withDependencies(DependencyGroups.Common)
  val Persistence = projectSettingsFactory.withDependencies(DependencyGroups.Persistence)
  val MicroService = projectSettingsFactory.withDependencies(DependencyGroups.MicroService)

  val WorkoutWebApp = projectSettingsFactory.withDependencies(DependencyGroups.WebApp)
  val AccountService = projectSettingsFactory.withDependencies(DependencyGroups.AuthService)

}
