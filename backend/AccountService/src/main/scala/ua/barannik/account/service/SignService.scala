package ua.barannik.account.service

import org.mindrot.jbcrypt.BCrypt
import ua.barannik.account.dao.UserDao
import ua.barannik.account.entity.User
import ua.barannik.common.validating._

case class LoginPass(login: String, pass: String)

trait SignService[T] extends AuthService[T] {

  def signUp(credentials: T): Result
  def signIn(credentials: T): ResultValue[UserName]

  override def authenticate(credentials: T) = signIn(credentials)
}

trait AuthFail
case class WrongLogin() extends AuthFail
case class WrongPass() extends AuthFail

trait SignUpFail
case class AlreadyExists() extends SignUpFail


class SignServiceImpl(userDao: UserDao) extends SignService[LoginPass] {
  override def signUp(credentials: LoginPass) = {
    if (userDao.find(credentials.login).isDefined)
      Fail(AlreadyExists())
    else {
      userDao.create {
        User(credentials.login, BCrypt.hashpw(credentials.pass, BCrypt.gensalt()))
      }
      Ok()
    }
  }

  override def signIn(credentials: LoginPass) = {
    val userOpt = userDao.find(credentials.login)
    userOpt.map { u =>
      if (BCrypt.checkpw(credentials.pass, u.pass))
        ValueOk(u.login)
      else
        Fail(WrongPass())
    }.getOrElse(Fail(WrongLogin()))
  }
}