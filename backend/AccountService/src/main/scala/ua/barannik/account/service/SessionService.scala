package ua.barannik.account.service
import java.util.Date

import ua.barannik.account.dao.SessionDao
import ua.barannik.account.entity.Session
import ua.barannik.common.validating._

import scala.util.Random

trait SessionService extends AuthService[String] {
  def start(login: String, pass: String): ResultValue[String]
  def stop(token: String)
}

case class NotSignedIn() extends AuthFail

class SessionServiceImpl(sessionDao: SessionDao, authService: AuthService[LoginPass]) extends SessionService {

  override def start(login: String, pass: String): ResultValue[String] = {
    authService.authenticate(LoginPass(login, pass)) match {
      case ValueOk(_) => {
        val expiresAt = new Date(System.currentTimeMillis() + SessionServiceImpl.SESSION_LIVE_TIME_MILLIS)
        val token = login.getBytes.map(b => java.lang.Integer.toString(b, 36)).mkString +
                    java.lang.Long.toString(System.currentTimeMillis(), 36) +
                    (1 to 3).map(v => java.lang.Long.toString(Random.nextLong(), 36)).mkString
        ValueOk(sessionDao.create(Session(login, token, expiresAt)))
      }
      case f @ Fail(_) => f
    }
  }

  override def stop(token: String) = sessionDao.remove(token)

  override def authenticate(token: String): ResultValue[UserName] =
    sessionDao.find(token)
      .map(s => ValueOk(s.user))
      .getOrElse(Fail(NotSignedIn()))
}

object SessionServiceImpl {
  val SESSION_LIVE_TIME_DAYS = 90
  val SESSION_LIVE_TIME_MILLIS: Long = SESSION_LIVE_TIME_DAYS.longValue() * 24 * 60 * 60 * 1000
}