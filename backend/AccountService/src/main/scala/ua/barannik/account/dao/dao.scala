package ua.barannik.account

import ua.barannik.account.entity.{Session, User}
import ua.barannik.persistance.dao.{Crud, ImmutableStore}

package object dao {
  type UserDao = Crud[User, String]
  type SessionDao = ImmutableStore[Session, String]
}
