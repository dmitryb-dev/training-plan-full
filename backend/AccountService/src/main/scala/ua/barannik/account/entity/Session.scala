package ua.barannik.account.entity

import java.util.Date

case class Session(user: String, token: String, expiresOn: Date)