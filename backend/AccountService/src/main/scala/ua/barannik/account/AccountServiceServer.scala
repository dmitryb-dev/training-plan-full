package ua.barannik.account

import akka.http.scaladsl.Http
import akka.http.scaladsl.server.Directives._
import ua.barannik.account.dao.RedisSessionDao
import ua.barannik.account.entity.User
import ua.barannik.account.service.{LoginPass, SessionServiceImpl, SignService, SignServiceImpl}
import ua.barannik.microservice.entry.ServerEntry
import ua.barannik.persistance.dao.LocalCrud
import ua.barannik.microservice.serialization.ResultJSONMarshaller.typedJsoniter
import ua.barannik.persistance.Redis

object AccountServiceServer extends ServerEntry {

  val signService: SignService[LoginPass] = new SignServiceImpl(LocalCrud[User, String](_.login))
  val sessionService = new SessionServiceImpl(new RedisSessionDao(Redis()), signService)

  signService.signUp(LoginPass("abc", "123"))

  val route =
    pathPrefix("account") {
      path("signin") {
        parameters('login, 'pass) { (login, pass) =>
          complete(sessionService.start(login, pass).map(Token))
        }
      } ~
      path("auth") {
        parameters('token) { token =>
          complete(sessionService.authenticate(token))
        }
      } ~
      path("signout") {
        parameters('token) { token =>
          sessionService.stop(token)
          complete("Done")
        }
      } ~
      path("signup") {
        parameters('login, 'pass).as(LoginPass) { credentials =>
          complete(signService.signUp(credentials))
        }
      }
    }

  val bindingFuture = Http().bindAndHandle(route, "localhost", 8082)
  info("Account Server started")
}

case class Token(value: String)
