package ua.barannik.account.actor

import akka.actor.Actor
import ua.barannik.account.service.{LoginPass, SignService}

case class SignUp(login: String, pass: String)

class AccountActor extends Actor {
  val signService: SignService[LoginPass] = ???

  override def receive = {
    case SignUp(login, pass) => sender ! signService.signUp(LoginPass(login, pass))
    case _ =>
  }
}
