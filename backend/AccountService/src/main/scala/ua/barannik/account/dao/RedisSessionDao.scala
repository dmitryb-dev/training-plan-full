package ua.barannik.account.dao
import java.util.Date

import ua.barannik.account.dao
import ua.barannik.account.entity.Session
import ua.barannik.persistance.RedisRequestPerformer
import ua.barannik.persistance.dao.Redis

class RedisSessionDao(redis: RedisRequestPerformer) extends dao.SessionDao {

  override def create(session: Session): String = redis.request(cl => {
    val key = SessionKey.fromToken(session.token)
    cl.set(key, session.user)
    cl.expire(key, (session.expiresOn.getTime - System.currentTimeMillis()).intValue())
    session.token
  })

  override def remove(token: String) = redis.request(cl => {
    cl.del(SessionKey.fromToken(token))
  })

  override def find(token: String): Option[Session] = redis.request(cl => {
    val key = SessionKey.fromToken(token)
    for (     user <- cl.get(key);
         expiresOn <- cl.ttl(key))
      yield Session(user, token, new Date(expiresOn))
  })

  private object SessionKey {
    val UserSessionPrefix = "us:"
    def fromToken(token: String) = UserSessionPrefix + token
  }
}
