package ua.barannik.account.service

import ua.barannik.common.validating.ResultValue

trait AuthService[T] {
  type UserName = String

  def authenticate(credentials: T): ResultValue[UserName]
}
