lazy val common = (project in file("libs/Common"))
  .settings(Settings.Common)

lazy val persistence = (project in file("libs/Persistence"))
  .dependsOn(common)
  .settings(Settings.Persistence)

lazy val microService = (project in file("libs/MicroService"))
  .dependsOn(common)
  .settings(Settings.MicroService)



lazy val gateway = (project in file("Gateway"))
  .dependsOn(microService)
  .settings(Settings.MicroService)

lazy val workoutGenerator = (project in file("WorkoutGenerator"))
  .dependsOn(microService)
  .settings(Settings.MicroService)

lazy val routineService = (project in file("RoutineService"))
  .dependsOn(microService, persistence, workoutGenerator)
  .settings(Settings.MicroService)

lazy val accountService = (project in file("AccountService"))
  .dependsOn(microService, persistence)
  .settings(Settings.AccountService)

lazy val historyService = (project in file("HistoryService"))
  .settings(Settings.MicroService)

lazy val liveSave = (project in file("LiveSave"))
  .settings(Settings.MicroService)

lazy val workoutWebApp = (project in file("WorkoutWebApp"))
  .dependsOn(workoutGenerator)
  .settings(Settings.WorkoutWebApp)
  .enablePlugins(PlayScala)


lazy val root = (project in file("."))
  .aggregate(common, workoutGenerator, accountService, routineService)