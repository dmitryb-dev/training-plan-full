package ua.barannik.routine.entity

import ua.barannik.persistance.LongId
import ua.barannik.generator.factory.ExerciseFactory
import ua.barannik.generator.entity.{ExerciseSet, Exercise => GeneratedExercise}
import ua.barannik.persistance.entity.{LongId, Named}

class Exercise[ES <: ExerciseSet](val id: Long, val name: String, val factory: ExerciseFactory[GeneratedExercise[ES]])
  extends LongId with Named
