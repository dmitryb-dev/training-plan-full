package ua.barannik.routine

import ua.barannik.persistance.dao.{Crud, FindAllFuture, FutureCrud}
import ua.barannik.generator.entity.ExerciseSet
import ua.barannik.routine.entity.{Day, Exercise, Workout}

package object dao {
  trait WorkoutDao extends FutureCrud[Workout, Long] with FindAllFuture[Workout]
  trait DayDao extends FutureCrud[Day, Long] with FindAllFuture[Day]
  trait ExerciseDao[ES <: ExerciseSet] extends FutureCrud[Exercise[ES], Long] with FindAllFuture[Exercise[ES]]
}
