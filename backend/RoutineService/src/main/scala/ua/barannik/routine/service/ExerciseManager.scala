package ua.barannik.routine.service

import ua.barannik.common.transform.FO
import ua.barannik.generator.entity.ExerciseSet
import ua.barannik.routine.dao.ExerciseDao
import ua.barannik.routine.entity.Exercise

import scala.concurrent.{ExecutionContext, Future}

trait ExerciseManager[ES <: ExerciseSet] extends ExerciseDao[ES] with FutureEntityLinks[Exercise[ES], Long] {
  implicit val context: ExecutionContext

  def createLink(id: Long) = ???

  def createCopy(id: Long): Future[Option[Exercise[ES]]] = {
    for (original <- FO(find(id));
           copyId <- create(original))
      yield Some(new Exercise[ES](copyId, original.name, original.factory))
  }
}
