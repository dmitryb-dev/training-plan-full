package ua.barannik.routine.service.provider

import ua.barannik.routine.service.WorkoutManager

trait WorkoutManagerProvider {
  def forUser(id: String): WorkoutManager
}
