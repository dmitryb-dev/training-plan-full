package ua.barannik.routine.entity

import ua.barannik.persistance.entity.{LongId, Named}
import ua.barannik.persistance.{LongId, UserProperty}

class ListEntity[T](val id: Long, val name: String, list: Seq[T]) extends Seq[T] with LongId with Named {

  override def length = list.length

  override def apply(idx: Int) = list.apply(idx)

  override def iterator = list.iterator
}
