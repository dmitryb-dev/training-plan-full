package ua.barannik.routine.dao.mongo

object User {
  val login = "_id"
  val pass = "pass"
  val workouts = "workouts"
}

object Workout {
  val id = "_id"
  val name = "name"
}
