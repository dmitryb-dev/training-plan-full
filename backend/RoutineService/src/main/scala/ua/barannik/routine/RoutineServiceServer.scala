package ua.barannik.routine

import akka.http.scaladsl.Http
import akka.http.scaladsl.server.Directives._
import ua.barannik.microservice.entry.ServerEntry
import ua.barannik.routine.dao.mongo.MongoWorkoutDao
import ua.barannik.routine.entity.Workout

object RoutineServiceServer extends ServerEntry {

  def workout(user: String) = {
    val workoutService = new MongoWorkoutDao(user)
    pathEndOrSingleSlash {
      onSuccess(workoutService.findAll()) { workouts =>
        complete(workouts.toString())
      }
    } ~
    path("create") {
      parameters("name") { name =>
        onSuccess(workoutService.create(new Workout(name))) { id =>
          complete("id = " + id)
        }
      }
    } ~
    path(LongNumber) { id =>
      onSuccess(workoutService.find(id)) { workout =>
        complete(workout.toString)
      }
    } ~
    path("remove" / LongNumber) { id =>
      workoutService.remove(id)
      complete("" + id)
    } ~
    path("update" / LongNumber) { id =>
      parameters("name") { name =>
        workoutService.update(id, new Workout(name))
        complete("id = " + id)
      }
    }
  }

  val route =
    pathPrefix("routine") {
      pathPrefix("workout") { workout("abc") }
    }

  Http().bindAndHandle(route, "localhost", 8083)
  info("Routine Server started")
}