package ua.barannik.routine.service

import ua.barannik.routine.dao.DayDao
import ua.barannik.routine.entity.Day

trait DayManager extends DayDao with EntityLinks[Day, Long] {
  def createLink(id: Long) = ???

  def createCopy(id: Long): Option[Day]
}
