package ua.barannik.routine.service

import ua.barannik.routine.entity.Exercise

import scala.concurrent.Future

trait EntityLinks[ENTITY, ID] {
  def createLink(id: ID)

  def createCopy(id: ID): Option[ENTITY]
}

trait FutureEntityLinks[ENTITY, ID] {
  def createLink(id: ID)

  def createCopy(id: ID): Future[Option[ENTITY]]
}