package ua.barannik.routine.entity

import ua.barannik.persistance.LongId
import ua.barannik.persistance.entity.{LongId, Named}

class Workout(val name: String, val id: Long = -1) extends LongId with Named