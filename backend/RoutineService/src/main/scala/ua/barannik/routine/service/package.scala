package ua.barannik.routine

import ua.barannik.routine.dao.WorkoutDao

package object service {
  type WorkoutManager = WorkoutDao
}
