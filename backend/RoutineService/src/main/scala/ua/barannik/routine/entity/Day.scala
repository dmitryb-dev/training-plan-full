package ua.barannik.routine.entity

import ua.barannik.persistance.LongId
import ua.barannik.persistance.entity.{LongId, Named}

class Day(val id: Long, val name: String, val loadPercent: Float) extends LongId with Named