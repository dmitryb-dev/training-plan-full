package ua.barannik.routine.dao.mongo

import org.mongodb.scala.bson.BsonTransformer._
import org.mongodb.scala.bson._
import org.mongodb.scala.model.{Filters, Projections, Updates}
import ua.barannik.common.transform.FO
import ua.barannik.common.transform.JustOne.requireOne
import ua.barannik.persistance.mongo.Mongo.RoutineDB
import ua.barannik.persistance.mongo.Parsers.BsonArrayParser
import ua.barannik.persistance.mongo.{Counters, MongoIdGenerator}
import ua.barannik.routine.entity.{Workout => WorkoutEntity}
import ua.barannik.routine.service.WorkoutManager

import scala.concurrent.{ExecutionContext, Future}

class MongoWorkoutDao(userId: String)(implicit context: ExecutionContext) extends WorkoutManager {

  RoutineDB.Users
    .insertOne(Document(User.login -> "abc")).toFuture().map(_ => 0)

  override def findAll() =
    RoutineDB.Users
      .find(Filters.eq(User.login, userId)).first()
      .toFuture()
      .map(user => user.get[BsonArray](User.workouts).get.parseDocuments {
        doc => new WorkoutEntity(doc.getString(Workout.name), doc.getLong(Workout.id))
      })

  override def create(entity: WorkoutEntity): Future[Long] = MongoIdGenerator.withIdFor(Counters.Workouts) { id =>
    create(id, entity)
  }

  override def remove(id: Long) =
    RoutineDB.Users
      .findOneAndUpdate(Filters.eq(User.login, userId), Updates.pull(User.workouts, Filters.eq(Workout.id, id)))
      .toFuture()
      .map(_ => id)

  override def update(id: Long, entity: WorkoutEntity) = {
    remove(id)
    create(id, entity)
  }

  override def find(id: Long): Future[Option[WorkoutEntity]] = {
    val workoutProjection = Projections.elemMatch(User.workouts, Filters.eq(Workout.id, id))
    val request = RoutineDB.Users
      .find(Filters.eq(User.login, userId))
      .projection(workoutProjection)
      .first()

    for (    user <- FO(request.toFutureOption());
         workouts <- user.get[BsonArray](User.workouts);
          workout <- workouts;
              doc  = new Document(workout.asDocument()))
      yield new WorkoutEntity(doc.getString(Workout.name), doc.getLong(Workout.id))
  }

  private def create(id: Long, entity: WorkoutEntity) = {
    val document = Document (
      Workout.id-> id,
      Workout.name -> entity.name
    )
    RoutineDB.Users
      .findOneAndUpdate(Filters.eq(User.login, userId), Updates.addToSet(User.workouts, document))
      .toFuture()
      .map(_ => id)
  }
}
