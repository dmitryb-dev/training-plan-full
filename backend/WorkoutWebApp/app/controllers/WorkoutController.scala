package controllers

import com.google.inject._
import entity.Link
import play.api.mvc.{AbstractController, ControllerComponents}
import ua.barannik.generator.entity.{Reps, WorkOnly}

@Singleton
class WorkoutController @Inject()(cc: ControllerComponents) extends AbstractController(cc) {
  def exercise(id: Long) = Action {
    val exercise = WorkOnly(Reps(3) :: Reps(2) :: Reps(1) :: Nil)
    Ok(views.html.workout("ex", exercise)(1))
  }
}