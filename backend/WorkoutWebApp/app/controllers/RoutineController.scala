package controllers

import com.google.inject._
import entity.Link
import play.api.mvc.{AbstractController, ControllerComponents}

/** It responses for workout scheduling. With pages of this controller you can see exercises and routines that you have
  * created.
  */
@Singleton
class RoutineController @Inject()(cc: ControllerComponents) extends AbstractController(cc) {
  def routines() = Action {
    val workouts = Link("First", 1) :: Link("Second", 2) :: Nil
    Ok(views.html.routines(workouts))
  }

  def days(routineId: Long) = Action {
    val days = Link("s", 1) :: Link("f", 2) :: Nil
    Ok(views.html.days("Routine 1")(days))
  }

  def exercises(dayId: Long) = Action {
    val ex = Link("1", 1) :: Link("2", 2) :: Nil
    Ok(views.html.exercises("Day 1", 1)(ex))
  }
}