package entity

class EntityLink[T](val name: String, val id: T)

case class Link(override val name: String, override val id: Long) extends EntityLink(name, id)