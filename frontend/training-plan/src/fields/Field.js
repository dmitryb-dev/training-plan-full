import React, { Component } from 'react';
import { FormGroup, ControlLabel, FormControl, HelpBlock } from 'react-bootstrap';

class FieldState {
  static noState = '';
  static warning = 'warning';
  static danger = 'danger';
  static success = 'success';
}

class Field extends Component {
    
  constructor(props) {
    super(props);
    
    this.validator = this.props.validator || function () { return FieldState.noState; }
    this.state = {
      value: ''
    }
  }
  
  handleChange(e) {
    this.setState({ 
      value: e.target.value 
    });
  }
  
  render() {
    return (
      <FormGroup controlId={this.props.children} validationState={this.validator()}>
      
        <ControlLabel>{this.props.children}</ControlLabel>
        <FormControl type={this.props.type || 'text'}
          value={this.state.value}
          placeholder={this.props.placeholder}
          onChange={this.handleChange.bind(this)}
        />
        {this.state.message? <HelpBlock>{this.state.message}</HelpBlock> : null}
         
      </FormGroup>
    );
  }
  
}
 
export { FieldState }
export default Field;