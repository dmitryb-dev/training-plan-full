import React from 'react';
import { FormGroup, ControlLabel, FormControl } from 'react-bootstrap';

function Select(props) {
  return (
    <FormGroup>
      <ControlLabel>{props.children}</ControlLabel>
      <FormControl componentClass="select" placeholder="select">
        <option value="select">select</option>
        <option value="other">...</option>
      </FormControl>
    </FormGroup>
  );
}

export default Select;