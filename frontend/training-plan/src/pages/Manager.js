import React, { Component } from 'react';
import Main from '../layout/Main'
import TrainingManager from '../manager/TrainingManager';

class ManagerPage extends Component {
  render() {
    return (
      <Main title="Training plan">
        <TrainingManager workouts={[1,2,3]} days={[4,5,6]} exercises={[7,8,9]} />
      </Main>
    );
  }
}

export default ManagerPage;