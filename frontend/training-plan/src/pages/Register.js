import React, { Component } from 'react';
import { Button } from 'react-bootstrap';
import StartPage from './Start';
import Field from '../fields/Field';

class RegisterPage extends Component {
  render() {
    return (
      <StartPage>
        <form>
          <Field>Email</Field>
          <Field>Password</Field>
          <Field>Pasword again</Field>
          <hr/>
          <Button bsStyle='primary' block>Sign up</Button>
        </form>
      </StartPage>
    );
  }
}

export default RegisterPage;