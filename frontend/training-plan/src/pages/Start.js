import React, { Component } from 'react';
import Title from '../layout/Title';
import Menu from '../layout/Menu';
import { Grid, Row, Col } from 'react-bootstrap';
import RegisterPage from './Register';
import LoginPage from './Login';

const signInLink = {
  name: 'Sign in',
  path: '/desktop/signin'
}
const signUpLink = {
  name: 'Sign up',
  path: '/desktop/signup'
}

function StartPage(props) {
  return (
    <Grid>
      <Row><Title>Getting started</Title></Row>
      <Row>
        <Menu>{[signInLink, signUpLink]}</Menu>
      </Row>
      <Row>
        <Col md={4} sm={6} mdOffset={4} smOffset={3}>
          {props.children}
        </Col>
      </Row>
    </Grid>
  );
}

export default StartPage;