import React, { Component } from 'react';
import { Button } from 'react-bootstrap';
import StartPage from './Start';
import Field from '../fields/Field';

function LoginPage(props) {
  return (
    <StartPage>
      <form>
        <Field>Email</Field>
        <Field>Password</Field>
        <hr/>
        <Button bsStyle='success' block>Sign in</Button>
      </form>
    </StartPage>
  );
}

export default LoginPage;