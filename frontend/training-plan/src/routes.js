import React from 'react';
import { BrowserRouter, Route, Switch, Redirect } from 'react-router-dom';

import LoginPage from './pages/Login';
import RegisterPage from './pages/Register';
import ManagerPage from './pages/Manager';
//import NotFound from './components/NotFound';

const Routes = (props) => (
  <BrowserRouter>
    <Switch>
      <Route exact path='/desktop/edit' component={ManagerPage} />
      <Route exact path='/desktop/signup' component={RegisterPage} />
      <Route exact path='/desktop/signin' component={LoginPage} />
      <Route path='/*' render={() => <Redirect to='/desktop/signin'/>} />
    </Switch>
  </BrowserRouter>
);

export default Routes;