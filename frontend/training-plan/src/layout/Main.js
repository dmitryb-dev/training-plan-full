import React from 'react';
import { Grid, Row } from 'react-bootstrap';
import Menu from './Menu';
import Title from './Title';
import './Main.css';

const editLink = {
  name: 'Edit',
  path: '/desktop/edit'
}
const historyLink = {
  name: 'History',
  path: '/desktop/history'
}
const workoutLink = {
  name: 'Workout',
  path: '/desktop/workout'
}
const logoutLink = {
  name: 'Sign out',
  path: '/desktop/signout'
}

function Main(props) {
  return (
    <Grid className="Main">
      <Row><Title>Training plan</Title></Row>
      <Row><Menu>{[editLink, historyLink, workoutLink, logoutLink]}</Menu></Row>
      <Row className='content'>{props.children}</Row>
    </Grid>
  );
}

export default Main;