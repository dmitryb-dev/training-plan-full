import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';
import List from '../manager/list/List';
import './MenuItem.css';
import './Menu.css';

function MenuItem(props) {
  return (
    <NavLink 
      to={props.children.path} 
      onClick={props.onClick} 
      activeClassName='MenuItem-active'
      className='MenuItem'
    >
      {props.children.name}
    </NavLink>
  );
}

class Menu extends Component {
  constructor(props) {
    super(props);
    this.listProps = {
      ...props,
      itemView: MenuItem
    }
  }
  
  render() {
    return (
      <div className='menu-wrapper'>
        <div className='Menu'>
          <List {...this.listProps} className='Menu-items'/>
          <hr className='line'/>
        </div>
      </div>
    )
  }
}

export default Menu;