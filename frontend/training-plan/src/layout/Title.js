import React from 'react';
import './Title.css';

function Title(props) {
  return (
    <div className='page-title' {...props}>
      <h3>{props.children}</h3>
    </div>
  );
}
  
export default Title;