import React, { Component } from 'react';
import ManagerPage from './pages/Manager';
import LoginPage from './pages/Login';

class App extends Component {
  render() {
    return (
      <div>
        <ManagerPage/>
        <LoginPage/>
      </div>
    );
  }
}

export default App;