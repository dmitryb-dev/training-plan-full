import React, { Component } from 'react';
import Scrollable from './list/Scrollable';
import TitledBlock from './list/TitledBlock';
import WorkoutProperties from './properties/WorkoutProperties';

import './PropertiesManager.css';


class PropertiesManager extends Component {
  render() {
    return (
      <TitledBlock className='PropertiesManager'>
        <Scrollable className='Content'>
          <WorkoutProperties/>
        </Scrollable>
      </TitledBlock>
    );
  }
}

export default PropertiesManager;