import React from 'react';
import Field from '../../fields/Field';
import Select from '../../fields/Select';
import { Button } from 'react-bootstrap';

function WorkoutProperties(props) {
  return (
    <div>
      <Select>Template</Select>
      <Field>Name</Field>
      <hr/>
      <Button bsStyle='danger' block>Remove</Button>
        
    </div>
  );
}

export default WorkoutProperties;