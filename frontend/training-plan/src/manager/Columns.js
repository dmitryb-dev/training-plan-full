import React from 'react';
import { Col } from 'react-bootstrap';

const BOOTSTRAP_MAX_COLUMNS_NUMBER = 12;

function Columns(props) {
  const children = props.children || [];
  const columnsNumber = children.reduce((number, child) => number + (child.props.colWR || 1), 0) || 1;
  const coluumnsRatio = Math.trunc(BOOTSTRAP_MAX_COLUMNS_NUMBER / columnsNumber) || 1;
  return (
    <div className={props.className || ''}>
      {children.map((child, i) => 
        BSColumnView(i, (child.props.colWR || 1) * coluumnsRatio, child, props)
      )}
    </div>
  );
}

function BSColumnView(index, width, child, props) {
  return (
    <Col key={index} md={width} sm={width}
      className={child.props.colClass || props.colClass || ''}>
        {child}
    </Col>
  );
}

export default Columns;