import React, { Component } from 'react';
import './List.css';

class List extends Component {
  constructor(props) {
    super(props);
    this.Item = this.props.itemView || 'div';
    
    this.state = {
      selected: undefined,
      values: this.props.children || [],
      disabled: this.props.disabled || false
    }
  }
  
  select(number) {
    if (number !== this.state.selected) {
      this.setState({
        ...this.state,
        selected: number
      });
      
      if (this.props.onChange) {
        this.props.onChange({
          list: this,
          index: number
        });
      }
    }
  }
  
  render() {
    return (
      <div className={'List ' + this.props.className || ''}>
        {this.state.values.map((value, i) => 
          <this.Item
            key={i}
            active={!this.state.disabled && this.state.selected === i}
            disabled={this.state.disabled}
            onClick={() => this.select(i)}>
              {value}
          </this.Item>
        )}
      </div>
    );
  }
}

export default List;