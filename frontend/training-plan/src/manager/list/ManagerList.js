import React, { Component } from 'react';
import Item from './Item';
import List from './List';
import Scrollable from './Scrollable';
import TitledBlock from './TitledBlock';
import { Button, Glyphicon } from 'react-bootstrap';

import './ManagerList.css';

class ManagerList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      disabled: this.props.disabled || false
    }
  }
  
  render() {
    return (
      <TitledBlock name={this.props.name} className='ManagerList'>
        <Scrollable className='Content'>
          <List itemView={Item} disabled={this.state.disabled}>{this.props.values}</List>
        </Scrollable>

        <Button bsStyle='success' block className='AddButton' disabled={this.state.disabled}>
          <Glyphicon glyph="plus" />
        </Button>
      </TitledBlock>
    );
  }
}

export default ManagerList;