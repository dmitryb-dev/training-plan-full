import React from 'react';

function TitledBlock(props) {
  return (
    <div className={props.className}>
      {props.name? <h3>{props.name}</h3> : null}
      {props.children}
    </div>
  );
}

export default TitledBlock;