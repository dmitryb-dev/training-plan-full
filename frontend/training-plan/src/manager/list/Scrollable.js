import React, { Component } from 'react';
import './Scrollable.css';

class Scrollable extends Component {
  
  render() {
    return (
      <div 
        className={'Scrollable ' + this.props.className || ''}
        style={{...this.props.style, height: this.props.height || 'inherit'}}>
          {this.props.children}
      </div>
    );
  }
}

export default Scrollable;