import React from 'react';
import { ListGroupItem } from 'react-bootstrap';
import './Item.css';

function Item(props) {
  return (
    <ListGroupItem className='Item' {...props} />
  );
}

export default Item;