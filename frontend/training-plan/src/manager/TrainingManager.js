import React, { Component } from 'react';
import ManagerList from './list/ManagerList';
import Columns from './Columns';
import PropertiesManager from './PropertiesManager';

import './TrainingManager.css';
import './manager-column.css';


class TrainingManager extends Component {
  render() {
    return (
      <div className='TrainingManager'>
        <Columns colClass='manager-column' className='columns-container'>
          <ManagerList values={this.props.workouts} name='Workouts'/>
          <ManagerList values={this.props.days} name='Days' disabled/>
          <ManagerList values={this.props.exercises} name='Exercises'/>
          <PropertiesManager />
        </Columns>
      </div>
    );
  }
}

export default TrainingManager;